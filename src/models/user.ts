/**
 * Created by Bunny on 06-07-2017.
 */
export interface User{
  email : string,
  password : string,
  userName : string,
  city : string,
  age : number,
  address : ['']
}
