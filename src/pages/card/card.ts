import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage} from '../home/home'
import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2/database'
import {DataProvider} from '../../Providers/todo-list-service'
import {EditProfilePage} from '../edit-profile/edit-profile'
declare var require: any;
import localForag from "localforage";
const localforage: LocalForage = require("localforage");
import {LoginPage} from '../login/login';
import {AngularFireAuth} from 'angularfire2/auth'
import {LoadingController,ToastController, AlertController } from 'ionic-angular';
/**
 * Generated class for the CardPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-card',
  templateUrl: 'card.html',
})
export class CardPage {
  users = [];
  deletionKey : any;
  index : any;
  loading : any;
  User:FirebaseListObservable<any[]>;
  constructor(public loadingCtrl: LoadingController,public alertCtrl: AlertController,public toastCtrl: ToastController,public navCtrl: NavController, public AFauth:AngularFireAuth,public navParams: NavParams,private angularfirdata: AngularFireDatabase,public DataProvider:DataProvider) {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

      this.loading.present();
    const queryObservable = this.angularfirdata.list('/user', {
      query: {
        orderByChild: 'id',
      }
    });
    queryObservable.forEach(element => {
      console.log(element);
      this.users = element
      this.loading.dismiss();
    });
    this.User = angularfirdata.list('/user');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CardPage');
  }
  moveToHomePage(id){
    this.DataProvider.UserInfo = this.users[id];
    var key =  this.users[id].$key;
    localforage.setItem("key",key);
    this.navCtrl.push(EditProfilePage)
  }
  logout() {

 
    this.AFauth.auth.signOut();
    localforage.removeItem("logIn").then((reset) => {
      localforage.setItem("logIn", false);
    })
    localforage.removeItem("people");
    localforage.removeItem("UserTodo");
    localforage.removeItem("key");
    this.navCtrl.setRoot(LoginPage);

}
presentToast(data) {
  let messgage= data;
  let toast = this.toastCtrl.create({
    message: messgage,
    duration: 3000
  });
  toast.present();
}
deleteUser(){
  if( Object.prototype.toString.call( this.DataProvider.UserTodo ) === '[object Array]' ) {
    this.deletionKey.delete = true;
    console.log(this.DataProvider.UserTodo[0]);
    this.User.update(this.deletionKey.$key, this.deletionKey).then((result)=>{
      this.DataProvider.flag = false;
      this.presentToast('user deleted succesfully')
    }).catch((err)=>{
      this.presentToast('something went wrong')
      console.log('something went wrong')
    });
  }
  else{
    this.deletionKey.delete = true;
    this.User.update(this.deletionKey.$key, this.deletionKey).then((result)=>{
      this.DataProvider.flag = false;
      this.presentToast('user deleted succesfully')
    }).catch((err)=>{
      this.presentToast('something went wrong')
      console.log('something went wrong')
    });
  }
}
presentConfirm(id) {
  this.deletionKey = this.users[id];
  let alert = this.alertCtrl.create({
    title: 'Are you sure?',
    message: 'After delete user is no longer able to login',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
         
        }
      },
      {
        text: 'Delete',
        handler: () => {
          this.deleteUser();
        }
      }
    ]
  });
  alert.present();
}
// deleteUser (){
//   console.log('called');
// }
}
