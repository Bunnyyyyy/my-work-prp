import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataProvider} from '../../Providers/todo-list-service'
declare var require: any;
import localForag from "localforage";
const localforage: LocalForage = require("localforage");
import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2/database'
import { LoadingController,ToastController, AlertController } from 'ionic-angular';
/**
 * Generated class for the EditProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
  userInfo : any;
  key : any;
  User:FirebaseListObservable<any[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController,public DataProvider:DataProvider,private angularfirdata: AngularFireDatabase) {
    this.userInfo = this.DataProvider.UserInfo;
    console.log(this.userInfo);
    this.User = angularfirdata.list('/user');

    localforage.getItem("key").then((result)=>{
      this.key = result;
    })

  }

  ionViewDidLoad() {
   
    console.log('ionViewDidLoad EditProfilePage');
  }
  presentToast(data) {
    let messgage= data;
    let toast = this.toastCtrl.create({
      message: messgage,
      duration: 3000
    });
    toast.present();
  }

  UpdateUser(){
    if( Object.prototype.toString.call( this.DataProvider.UserTodo ) === '[object Array]' ) {
      this.DataProvider.UserTodo[0] = this.userInfo;
      console.log(this.DataProvider.UserTodo[0]);
      this.User.update(this.key, this.DataProvider.UserTodo[0]).then((result)=>{
        this.DataProvider.flag = false;
        console.log(this.key)
        console.log(result);
        this.presentToast('data is store succesfully')
      }).catch((err)=>{
        this.presentToast('something went wrong')
        console.log('something went wrong')
      });
    }
    else{
      this.DataProvider.UserTodo = this.userInfo;
      console.log(this.DataProvider.UserTodo);
      this.User.update(this.key, this.DataProvider.UserTodo).then((result)=>{
        this.DataProvider.flag = false;
        console.log(result);
        console.log(this.key)
        this.presentToast('data is store succesfully')
      }).catch((err)=>{
        this.presentToast('something went wrong')
        console.log('something went wrong')
      });
    }
    localforage.setItem("people", this.userInfo);
  }
  
}
