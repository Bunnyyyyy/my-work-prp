import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import {HomePage} from '../home/home'
import {AngularFireAuth} from 'angularfire2/auth'
import {User} from '../../models/user'
import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2/database'
import {DataProvider} from '../../Providers/todo-list-service'
declare var require: any;
import localForag from "localforage";
const localforage: LocalForage = require("localforage");
import {CardPage} from '../card/card'
import { from } from 'rxjs/observable/from';
import {ToastController, AlertController } from 'ionic-angular';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage
 {
  signUp = false;
  ToDoList:FirebaseListObservable<any[]>;
  list : FirebaseObjectObservable<any[]>;
  showTodo = [];
  err = false;
  ErrMsg = "";
  Usertodo = {
    'id': null,
    'userName': null,
    'age': null,
    'city': null,
    'address' : [''],
    'type' : null
  };
  checkBox = false;
  todoArray = [];
  loading : any;
   user = {} as User;
   address = [];
   
  constructor(public loadingCtrl: LoadingController,public toastCtrl: ToastController,public DataProvider:DataProvider, public AFauth:AngularFireAuth,public navCtrl: NavController, public navParams: NavParams, private angularfirdata: AngularFireDatabase) {
    this.ToDoList = angularfirdata.list('/user');
    console.log(this.showTodo);
    this.user.address = ['']

    this.address = [''];
  }
  presentToast(data) {
    let messgage= data;
    let toast = this.toastCtrl.create({
      message: messgage,
      duration: 3000
    });
    toast.present();
  }

  async signup(user : User){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    try {
      this.loading.present();
      const result = await this.AFauth.auth.createUserWithEmailAndPassword(user.email, user.password);
      console.log(result.uid);
      this.Usertodo.id = result.uid;
      this.Usertodo.userName = this.user.userName;
      this.Usertodo.age = this.user.age;
      this.Usertodo.city = this.user.city;
      this.Usertodo.address = this.address;
      if(this.checkBox){
        this.Usertodo.type = 'admin';
      }else{
        this.Usertodo.type = 'user';
      }
      console.log(result);
      console.log(this.Usertodo);
      this.ToDoList.push(this.Usertodo);
      const queryObservable = this.angularfirdata.list('/user', {
        query: {
          orderByChild: 'id',
          equalTo: result.uid
        }
      });
      this.todoArray = [];
      queryObservable.forEach(element => {
        if( Object.prototype.toString.call( this.todoArray ) === '[object Array]' ) {
          this.todoArray.push(element);
          console.log(element);
          var key = element[0].$key;
          console.log(key);
          localforage.setItem("key",key);
        }
        else {
          console.log(element);
          this.todoArray = element;
          // var key = element.$key
          console.log(key)
        }
        console.log(element);
        this.todoArray = this.todoArray[0];
        console.log(this.todoArray);
        this.DataProvider.UserTodo = this.todoArray;
        this.loading.dismiss();
        localforage.setItem("UserTodo", this.todoArray);
        localforage.setItem("logIn", true);
        if(this.Usertodo.type == 'admin'){
          this.navCtrl.setRoot(CardPage);
          localforage.setItem("admin", true);
        }else{
        this.navCtrl.setRoot(HomePage);
        localforage.setItem("admin", false);
        }
      });
    }catch (e){
      this.err = true;
      this.loading.dismiss();
      console.log(e);
      this.ErrMsg = e.message;
    }
  }
  async login(user : User){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    try {
      this.loading.present();
      const result = await this.AFauth.auth.signInWithEmailAndPassword(user.email, user.password);
      console.log(result);
      const queryObservable = this.angularfirdata.list('/user', {
        query: {
          orderByChild: 'id',
          equalTo: result.uid
        }
      });
      this.todoArray = [];
      queryObservable.forEach(element => {
        console.log(this.todoArray);
        if( Object.prototype.toString.call( this.todoArray ) === '[object Array]' ) {
          this.todoArray.push(element);
          var key = element[0].$key;
          localforage.setItem("key",key);
        }
        else {
          console.log('else');
          this.todoArray = element;
          // var key = element.$key;
          // localforage.setItem("key",key);
        }
        console.log(element);
        this.todoArray = this.todoArray[0];
        console.log(this.todoArray);
        this.DataProvider.UserTodo = this.todoArray;
        this.loading.dismiss();
        if(this.todoArray[0].delete){
          this.presentToast('You are deleted from user list please sign up again with diffrent email to proceed');
          return;
          // this.navCtrl.setRoot(CardPage);
          // localforage.setItem("admin", true);
        }
        localforage.setItem("UserTodo", this.todoArray);
        localforage.setItem("logIn", true);
        console.log(this.todoArray);
        if(this.todoArray[0].type == 'admin'){
          this.navCtrl.setRoot(CardPage);
          localforage.setItem("admin", true);
        }else{
        this.navCtrl.setRoot(HomePage);
        localforage.setItem("admin", false);
        }
        localforage.setItem("key",result.uid)
      });
      console.log(this.todoArray);
      console.log(result.m);
      console.log(queryObservable);
    }catch (e){
      this.err = true;
      this.loading.dismiss();
      console.log(e);
      this.ErrMsg = e.message;
    }
  }
  addMoreAddress(){
    this.address.push('');
    this.user.address.push('');
  }
  ShowSignUp(data){
    this.user.email = "";
    this.user.password = "";
    console.log('skip log in called');
   this.signUp = data;
  }
  ionViewDidLoad() {
    
    console.log('ionViewDidLoad LoginPage');
  }
}
