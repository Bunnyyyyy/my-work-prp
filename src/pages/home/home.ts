import { Component } from '@angular/core';
import { NavController,LoadingController,ToastController, AlertController } from 'ionic-angular';
import {AngularFireAuth} from 'angularfire2/auth'
import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2/database'
import {DataProvider} from '../../Providers/todo-list-service'
declare var require: any;
import localForag from "localforage";
const localforage: LocalForage = require("localforage");
import {LoginPage} from '../login/login';
import { Injectable, Pipe } from '@angular/core';
import {EditProfilePage} from '../edit-profile/edit-profile'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  UserInfoshow = {};
  constructor(public alertCtrl: AlertController, public AFauth:AngularFireAuth,public toastCtrl: ToastController,public DataProvider:DataProvider,public navCtrl: NavController, private angularfirdata: AngularFireDatabase) {
    localforage.getItem("UserTodo").then((result)=>{
      if(result){
        DataProvider.UserTodo = result[0];
      }
      console.log(DataProvider.UserTodo);
      this.UserInfoshow = this.DataProvider.UserTodo;
      console.log(this.UserInfoshow)
    })
}
updateuser(){
  this.DataProvider.UserInfo = this.UserInfoshow;
  // var key =  this.users[id].$key;
  // localforage.setItem("key",key);
  this.navCtrl.push(EditProfilePage)
}
logout() {

 
    this.AFauth.auth.signOut();
    localforage.removeItem("logIn").then((reset) => {
      localforage.setItem("logIn", false);
    })
    localforage.removeItem("people");
    localforage.removeItem("UserTodo");
    localforage.removeItem("key");
    this.navCtrl.setRoot(LoginPage);

}

}
