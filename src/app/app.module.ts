import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AngularFireModule } from "angularfire2";
import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2/database'
import {LoginPage} from '../pages/login/login';
import {DataProvider} from '../Providers/todo-list-service'
import {AngularFireAuthModule} from 'angularfire2/auth'
import {CardPage} from '../pages/card/card'
import {EditProfilePage} from '../pages/edit-profile/edit-profile'
export const firebaseConfig = {
  apiKey: "AIzaSyDygBmD4qHq-CIjmGDOp_m7q5aK0oXo2so",
  authDomain: "todo-956f6.firebaseapp.com",
  databaseURL: "https://todo-956f6.firebaseio.com",
  projectId: "todo-956f6",
  storageBucket: "todo-956f6.appspot.com",
  messagingSenderId: "88542137692"
};
@NgModule({
  declarations: [
    MyApp,
    CardPage,
    HomePage,
    LoginPage,
    EditProfilePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireAuthModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    CardPage,
    EditProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    DataProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
