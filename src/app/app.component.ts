import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import {LoginPage} from '../pages/login/login'
declare var require: any;
import localForag from "localforage";
const localforage: LocalForage = require("localforage");
import {CardPage} from '../pages/card/card'
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      localforage.getItem("logIn").then((result) => {
        var people = result ;
        console.log(result);
        console.log(people);
        if(people === true){
          localforage.getItem("admin").then((admin) => {
            if(admin === true){
              this.rootPage = CardPage;
          }
          else{
            this.rootPage = HomePage;
          }
          });
        }
        else{
          this.rootPage = LoginPage;

        }
      }, (error) => {
        console.log('error');
        console.log("ERROR: ", error);
        statusBar.styleDefault();
        splashScreen.hide();
      });
    });

  }

}

