/**
 * Created by Bunny on 06-07-2017.
 */
import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataProvider {
  todoList : any;
  TodoObservable : any;
  list : any;
  userKey : any;
  UserTodo : any;
  index : any;
  Title : any;
  providerShowTodo : any;
  flag : any;
  showTodo: any;
  editTaskFlag = true;
  UserInfo : any;
}
